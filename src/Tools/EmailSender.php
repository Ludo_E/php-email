<?php

namespace Afpa\Tools;

use PHPMailer\PHPMailer\PHPMailer;

class EmailSender
{

    /**
     * Fonction permettant d'envoyer un email
     * 
     * @param $subject L'objet de l'email
     * @param $body Le corps du message
     * @param $recipient Le destinataire de l'email
     */
    public function sendEmail(string $subject, string $body, string $recipient)
    {
        $mail = new PHPMailer();

        $mail->isHTML(true);

        // TODO à compléter pour envoyer un email
        // vous pourrez vous inspirer du tutoriel suivant : https://www.geeksforgeeks.org/how-to-send-an-email-using-phpmailer/

        // pour tester votre code vous pourrez utiliser le serveur d'email de test : https://www.wpoven.com/tools/free-smtp-server-for-testing
        // pensez à adapter le paramétrage du serveur

        // TODO ajouter une image "embarquée" en guise de signature
        // vous pourrez vous aider de la question suivante : https://stackoverflow.com/questions/3708153/send-email-with-phpmailer-embed-image-in-body
    }

}