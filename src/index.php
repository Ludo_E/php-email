<?php
    use Afpa\Tools\EmailSender;
?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Projet de test d'envoi d'email</title>
</head>
<body>
    <?php
    // les lignes suivantes permettent de faire appel à la classe d'envoi d'email
    // TODO compléter la méthode "sendEmail" de la classe EmailSender et l'utiliser à bon escient
    $emailSender = new EmailSender();
    $emailSender->sendEmail("subject", "body", "adresse-destination");
    ?>

    <form>
        <!-- TODO : créer un formulaire contenant les informations nécessaires à l'envoi d'emails :
            - sujet de l'email
            - zone de texte pour le corps du message
            - adresse du destinataire
        -->
    </form>


</body>
</html>